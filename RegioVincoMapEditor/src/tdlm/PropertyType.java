/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tdlm;

/**
 *
 * @author McKillaGorilla
 */
public enum PropertyType {
    WORKSPACE_HEADING_LABEL,
    
    DETAILS_HEADING_LABEL,
    NAME_PROMPT,
    OWNER_PROMPT,

    ITEMS_HEADING_LABEL,
    
    CHANGE_NAME_ICON,
    CHANGE_NAME_TOOLTIP,
    
    ADD_ICON,
    ADD_ICON_TOOLTIP,
    
    CATEGORY_COLUMN_HEADING,
    DESCRIPTION_COLUMN_HEADING,
    START_DATE_COLUMN_HEADING,
    END_DATE_COLUMN_HEADING,
    COMPLETED_COLUMN_HEADING,
    
    MAP,
}
