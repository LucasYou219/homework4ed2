package tdlm.gui;

import java.io.IOException;
import java.time.LocalDate;
import javafx.beans.binding.Bindings;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import tdlm.controller.ToDoListController;
import tdlm.data.DataManager;
import saf.ui.AppYesNoCancelDialogSingleton;
import saf.ui.AppMessageDialogSingleton;
import properties_manager.PropertiesManager;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
import static saf.settings.AppStartupConstants.PATH_IMAGES;
import tdlm.PropertyType;
import tdlm.data.Subregions;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS Workspace'S COMPONENTS TO A STYLE SHEET THAT IT USES
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;

    AppTemplate app;

    AppGUI gui;

    ToDoListController toDoListController;
    
    Label headingLabel;
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;
    
    //THE ELEMENTS
    //BorderPane borderpane = new BorderPane();
    SplitPane splitpane = new SplitPane();
    HBox editToolbar = new HBox(10);
    VBox itembox = new VBox();
    VBox mapbox = new VBox();
    
    Button changeName;
    Button addImage;
    Button removeImage;
    Button changeBGColor;
    Button ChangeBorderColor;
    Button reassignColor;
    Button changeDimensions;
    
    Image map;
    ImageView imageView = new ImageView();
    
    Slider changeBorderThickness;
    Slider zoomLevel;
    TableView subregionTable = new TableView();
    TableColumn subregionsColumn;
    
    TableColumn itemCategoryColumn;
    TableColumn itemDescriptionColumn;
    TableColumn itemStartDateColumn;
    TableColumn itemEndDateColumn;
    TableColumn itemCompletedColumn;
    
    // FOR DISPLAYING DEBUG STUFF
    Text debugText;

    public Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();

        // INIT ALL WORKSPACE COMPONENTS
	layoutGUI();
        
        // AND SETUP EVENT HANDLING
	setupHandlers();
    }
    
    private void layoutGUI() 
    {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        DataManager dataManager = (DataManager)app.getDataComponent();
        
        
        
        //SETUP TOOLBAR
        changeName = gui.initChildButton(editToolbar, PropertyType.ADD_ICON.toString(), PropertyType.ADD_ICON_TOOLTIP.toString(), false);
        addImage = gui.initChildButton(editToolbar, PropertyType.ADD_ICON.toString(), PropertyType.ADD_ICON_TOOLTIP.toString(), false);
        removeImage = gui.initChildButton(editToolbar, PropertyType.ADD_ICON.toString(), PropertyType.ADD_ICON_TOOLTIP.toString(), false);
        changeBGColor = gui.initChildButton(editToolbar, PropertyType.ADD_ICON.toString(), PropertyType.ADD_ICON_TOOLTIP.toString(), false);
        reassignColor = gui.initChildButton(editToolbar, PropertyType.ADD_ICON.toString(), PropertyType.ADD_ICON_TOOLTIP.toString(), false);
        changeDimensions = gui.initChildButton(editToolbar, PropertyType.ADD_ICON.toString(), PropertyType.ADD_ICON_TOOLTIP.toString(), false);
     
        
        //SETUP MAP
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + "Map.png";
        map = new Image(imagePath);
        imageView.setImage(map);
        
        //SETUP TABLE
        subregionsColumn = new TableColumn("Subregions");
        subregionsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("subregions"));
        //subregionsColumn.setPrefWidth(subregionTable.getPrefWidth());
        subregionsColumn.prefWidthProperty().bind(subregionTable.widthProperty());
        subregionTable.getColumns().add(subregionsColumn);
        
        subregionTable.setPrefHeight(575);
        //ToDoItem subregion = new ToDoItem();
        //Subregions subregion = new Subregions("some city");
        //dataManager.addItem(subregion);
        subregionTable.setItems(dataManager.getItems());
        
        
        itembox.getChildren().add(subregionTable);
        mapbox.getChildren().add(imageView);
        splitpane.getItems().addAll(mapbox,itembox);
        workspace = new VBox();
        workspace.getChildren().add(editToolbar);
        workspace.getChildren().add(splitpane);
        
    }
    
    public void setDebugText(String text) {
	debugText.setText(text);
    }
    
    
    private void setupHandlers() {
	// MAKE THE CONTROLLER
	toDoListController = new ToDoListController(app);
	
	
    }
    
    public void setImage(ButtonBase button, String fileName) {
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + fileName;
        Image buttonImage = new Image(imagePath);
	
	// SET THE IMAGE IN THE BUTTON
        button.setGraphic(new ImageView(buttonImage));	
    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {

        itembox.getStyleClass().add(CLASS_BORDERED_PANE);
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
	//DataManager dataManager = (DataManager)app.getDataComponent();
        
        

    }
}
